# okglauben_volkenroda_18

## Projekt OnlineAndacht Kloster Volkenroda 2018

Informationen zum Projekt auf der Website der [OnlineKirche](https://onlinekirche.ekmd.de/projekte/onlineandacht-jugendfestival-volkenroda-2018.html)

Special Thanx to [BlackrockDigital](https://github.com/BlackrockDigital/startbootstrap-one-page-wonder)

## Prevent access to git data

Rename the file _htaccess to .htaccess, if a directive for deny access git data is not present in a generell .htaccess, maybe in the root of public html data. Make sure the RewriteEngine of apache is active.
